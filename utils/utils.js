var env = process.env.NODE_ENV || 'development';
var config = require('./../config/config.json')[env];
var bcrypt = require('bcrypt');
var crypto = require('crypto');

module.exports.generateKey = function () {
    return crypto.randomBytes(32).toString('hex');
};
module.exports.generateWalletPass = function () {
    return Math.random().toString(36).slice(-32);
};
module.exports.generatePass = function () {
    return Math.random().toString(36).slice(-8);
};

module.exports.generateFakeAddress =  function() {
    return '0x' + crypto.randomBytes(20).toString('hex').toUpperCase();
};