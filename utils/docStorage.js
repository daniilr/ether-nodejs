var fs = require('fs');
var os = require('os');
var path = require('path');
var crypto = require('crypto');
var mkdirp = require('mkdirp');
var mime = require('mime');
var uuid_lib = require('node-uuid');

function getFilename(req, file, cb) {
    var uuid = uuid_lib.v4();
    cb(null, uuid + '.' + mime.extension(file.mimetype),file.mimetype, file.filename, uuid);
}

function getDestination(req, file, cb) {
    cb(null, os.tmpdir())
}

function DiskStorage(opts) {
    this.getFilename = (opts.filename || getFilename);

    if (typeof opts.destination === 'string') {
        mkdirp.sync(opts.destination);
        this.getDestination = function ($0, $1, cb) {
            cb(null, opts.destination)
        }
    } else {
        this.getDestination = (opts.destination || getDestination)
    }
}

DiskStorage.prototype._handleFile = function _handleFile(req, file, cb) {
    var that = this;
    var hash = crypto.createHash('sha256');
    that.getDestination(req, file, function (err, destination) {
        if (err) return cb(err);

        that.getFilename(req, file, function (err, filename,mimetype, original, uuid) {
            if (err) return cb(err);

            var finalPath = path.join(destination, filename);
            var outStream = fs.createWriteStream(finalPath);

            file.stream.pipe(outStream);
            outStream.on('error', cb);
            outStream.on('data', function (chunk) {
                hash.update(chunk)
            });
            outStream.on('finish', function () {
                cb(null, {
                    destination: destination,
                    filename: filename,
                    original_filename: original,
                    path: finalPath,
                    uuid: uuid,
                    mimetype: mimetype,
                    size: outStream.bytesWritten,
                    hash: hash.digest('hex')
                })
            })
        })
    })
};

DiskStorage.prototype._removeFile = function _removeFile(req, file, cb) {
    var path = file.path;

    delete file.destination;
    delete file.filename;
    delete file.path;

    fs.unlink(path, cb)
};

module.exports = function (opts) {
    return new DiskStorage(opts)
};