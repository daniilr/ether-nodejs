var models = require('../models');
var response = require('./index');
var consts = require('../utils/consts');
var diskStorage = require('../utils/imageStorage');
module.exports.storageLogo = diskStorage({
    destination: function (req, file, cb) {
        cb(null, './public/uploads/logo/original');
    }
});

module.exports.deleteLogo = function (req, res) {
    models.Company.findOne(
        {
            where: {id: req.user.company},
            include: [
                {model: models.Logo}
            ]
        }).then(function (company) {
        if (company.Logo) {
            company.Logo.destroy().then(function () {
                response.successFuncF(res, consts.LOGO_DELETED, null);
            }, function (err) {
                response.errDbFunc(res, err);
            })
        } else {
            response.errorFunc(res, consts.CODE.NOT_FOUND, consts.LOGO_NOT_FOUND)
        }
    });
};

module.exports.uploadLogo = function (req, res) {
    var dest =  req.file.path.replace('public','');

    models.Logo.create({
        hash: req.file.hash,
        original: dest,
        small: dest,
        normal: dest,
        big: dest
    }).then(function (logo) {
        models.Company.findOne({
            where: {id: req.user.company}
        }).then(function (company) {
            company.setLogo(logo).then(function () {
                response.successFuncS(res, logo);
            })
        });
    }, function (err) {
        response.errDbFunc(res, err);
    })

};