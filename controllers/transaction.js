var models = require('../models');
var response = require('./index');
var consts = require('../utils/consts');
var Sequelize = require("sequelize");
var sequelize = require('../models').sequelize;
var diskStorage = require('../utils/docStorage');
var _ = require("underscore");

module.exports.storageDocument = diskStorage({
    destination: function (req, file, cb) {
        cb(null, './public/uploads/doc');
    }
});

module.exports.createTransaction = function (req, res) {
    console.log("create transaction");
    if (req.body.members && req.body.document) {
        if (req.body.members.length > 0) {
            Sequelize.Promise.all([
                models.Company.findOne({where: req.user.company}),
                models.Company.findAll({where: {id: req.body.members}}),
                models.File.findAll({
                    where: {
                        id: req.body.document.files,
                        UserId: req.user.id
                    }
                }),
                models.Transaction.create({
                    type: req.body.type
                }),
                models.Document.create({
                    name: req.body.document.name, //Полное наименование
                    sign: req.body.document.sign //Дата составления
                })
            ]).spread(function (owner, members, files, transaction, document) {
                members.push(owner);
                Sequelize.Promise.map(members, function (member) {
                    return models.Condition.create({CompanyId: member.dataValues.id})
                }).then(function (conditions) {
                    Sequelize.Promise.map(members, function (member) {
                        return models.Decision.create({
                            CompanyId: member.dataValues.id,
                            action: member.dataValues.id == req.user.company ? "approve" : "pending"
                        })
                    }).then(function (decisions) {
                        Sequelize.Promise.all([
                            document.setFiles(files),
                            transaction.setDocument(document),
                            transaction.setConditions(conditions),
                            transaction.setDecisions(decisions),
                            transaction.setMembers(members),
                            transaction.setOwnerTransaction(owner)
                        ]).spread(function (document, transaction) {
                            models.Transaction.findOne(
                                {
                                    where: {id: transaction.dataValues.id},
                                    include: [
                                        {all: true},
                                        {model: models.Document, include: models.File}
                                    ]
                                }
                            ).then(function (updateTransaction) {
                                response.successFuncS(res, updateTransaction);
                            });
                        });
                    });
                });

            }, function (err) {
                response.errDbFunc(res, err);
            });
        } else {
            response.errorFunc(res, consts.CODE.NOT_FOUND, consts.MEMBERS_NOT_FOUND);
        }
    } else {
        response.errorFunc(res, consts.CODE.BAD_REQUEST, consts.BAD_REQUEST)
    }
};

module.exports.uploadFile = function (req, res) {
    var dest = req.file.path.replace('public', '');
    models.File.create(
        {
            uuid: req.file.uuid,
            filename: req.file.originalname,
            hash: req.file.hash,
            size: req.file.size,
            mimetype: req.file.mimetype,
            destination: dest
        }
    ).then(function (doc) {
        doc.setUser(req.user.id).then(function (doc) {
            response.successFuncS(res, doc);
        }, function (err) {
            response.errDbFunc(res, err);
        });
    }, function (err) {
        response.errDbFunc(res, err);
    });

};

module.exports.deleteTransaction = function (req, res) {
    if (req.params.transactionId) {
        models.Transaction.findOne({
            where: {
                id: req.params.transactionId, status: "pending"
            }
        }).then(function (transaction) {
            if (transaction) {
                transaction.destroy().then(function (deleted_transaction) {
                    if (deleted_transaction && deleted_transaction.deletedAt) {
                        response.successFuncS(res, null);
                    }
                });
            } else {
                response.errorFunc(res, consts.CODE.NOT_FOUND, consts.TRANSACTION_NOT_FOUND);
            }
        });
    } else {
        response.errorFunc(res, consts.CODE.BAD_REQUEST, consts.QUERY_NOT_FOUND);
    }
};

module.exports.decisionTransaction = function (req, res) {
    console.log("decisionTransaction");
    if (req.params.transactionId && req.body.action) {

        models.Transaction.findOne({
            where: {id: req.params.transactionId, status: "pending"},
            include: [{model: models.Decision, as: 'Decisions'}]
        }).then(function (transaction) {
            if (transaction) {
                var decision = _.find(transaction.Decisions, {CompanyId: req.user.company});

                if (decision) {
                    decision.updateAttributes({
                        action: req.body.action,
                        comment: req.body.comment
                    }).then(function () {
                        var decisionLength = transaction.Decisions.length;
                        var approveLength = _.where(transaction.Decisions, {action: "approve"}).length;
                        var declineLength = _.where(transaction.Decisions, {action: "decline"}).length;
                        var transactionStatus = "pending";
                        if (decisionLength === approveLength || declineLength > 0) {
                            if (decisionLength === approveLength) {
                                transactionStatus = "approve";
                            } else if (declineLength > 0) {
                                transactionStatus = "decline";
                            }
                            transaction.updateAttributes({
                                    status: transactionStatus
                                }
                            ).then(function (transaction) {
                                response.successFuncS(res, transaction);
                            })
                        } else {

                            response.successFuncS(res, transaction);
                        }
                    }, function (err) {
                        response.errorFunc(res, consts.CODE.FORBIDDEN, err.message);
                    });
                } else {
                    response.errorFunc(res, consts.CODE.BAD_REQUEST, consts.DECISION_NOT_FOUND);
                }
            } else {
                response.errorFunc(res, consts.CODE.NOT_FOUND, consts.TRANSACTION_NOT_FOUND);
            }
        })
    } else {
        response.errorFunc(res, consts.CODE.BAD_REQUEST, consts.QUERY_NOT_FOUND);
    }
};

module.exports.conditionTransaction = function (req, res) {
    console.log("conditionTransaction");
    if (req.params.transactionId && req.body.condition) {
        models.Transaction.findOne({
            where: {id: req.params.transactionId, status: "approve"},
            include: [{model: models.Condition, as: 'Conditions'}, {model: models.Decision, as: 'Decisions'}]
        }).then(function (transaction) {
                if (transaction) {
                    var condition = _.find(transaction.Conditions, {CompanyId: req.user.company});
                    if (condition) {
                        condition.updateAttributes({
                            condition: req.body.condition,
                            comment: req.body.comment
                        }).then(function () {
                            transaction.updateAttributes({
                                    condition: req.body.condition,
                                    comment: req.body.comment
                                }
                            ).then(function (transaction) {
                                response.successFuncS(res, transaction);
                            })
                        });
                    } else {
                        response.errorFunc(res, consts.CODE.BAD_REQUEST, consts.CONDITION_NOT_FOUND);
                    }
                } else {
                    response.errorFunc(res, consts.CODE.NOT_FOUND, consts.TRANSACTION_NOT_FOUND);
                }
            }
            ,
            function (err) {
                response.errorFunc(res, consts.CODE.FORBIDDEN, err.message);
            }
        );
    }
};

module.exports.getTransaction = function (req, res) {
    if (req.params.transactionId) {
        models.Transaction.findOne(
            {
                where: {id: req.params.transactionId},
                include: [
                    {all: true},
                    {model: models.Document, include: models.File}
                ]
            }
        ).then(function (transaction) {
            if (transaction) {
                response.successFuncS(res, transaction);
            } else {
                response.errorFunc(res, consts.CODE.NOT_FOUND, consts.TRANSACTION_NOT_FOUND);
            }
        }, function (err) {
            response.errDbFunc(res, err);
        });
    } else {
        response.errorFunc(res, consts.CODE.BAD_REQUEST, consts.QUERY_NOT_FOUND);
    }

};
module.exports.getTransactions = function (req, res) {
    var companyId = req.params.companyId ? req.params.companyId : req.user.company;
    models.MemberTransaction.findAll(
        {
            attributes: ['transactionId'],
            where: {'companyId': companyId}
        }
    ).then(function (transactionIds) {
        var transactionIdsArray = [];
        _.each(transactionIds, function (obj) {
            if (obj) {
                transactionIdsArray.push(obj.dataValues.transactionId);
            }
        });
        if (transactionIdsArray && transactionIdsArray.length > 0) {
            models.Transaction.findAll(
                {
                    where: {id: transactionIdsArray},
                    include: [
                        {model: models.Document, include: models.File},
                        {model: models.Decision, as: 'Decisions'},
                        {model: models.Condition, as: 'Conditions'},
                        {model: models.Company, as: 'Members'}]
                }
            ).then(function (transactions) {
                if (transactions && transactions.length > 0) {
                    response.successFuncS(res, transactions);
                } else {
                    response.errorFunc(res, consts.CODE.NOT_FOUND, consts.TRANSACTIONS_NOT_FOUND);
                }
            }, function (err) {
                response.errDbFunc(res, err);
            });
        } else {
            response.errorFunc(res, consts.CODE.NOT_FOUND, consts.TRANSACTIONS_NOT_FOUND);
        }

    }, function (err) {
        response.errDbFunc(res, err);
    });

};

module.exports.globalSearch = function (req, res) {
    if (req.query.q) {
        var query = req.query.q;
        Sequelize.Promise.all([
            sequelize.query('SELECT * FROM "Companies" ' +
                'WHERE id IN (with data_company as (SELECT id, element.value FROM (SELECT id,data from "Companies") jt, jsonb_each_text(jt.data) as element) ' +
                'SELECT id FROM data_company WHERE value ILIKE :q) OR name ILIKe :q',
                {
                    model: models.Company,
                    include: [{all: true}],
                    replacements: {"q": "%" + query + "%"}
                }),

            models.Transaction.findAll({
                where: {
                    $or: [
                        {
                            addressBlock: {
                                $iLike: '%' + query + '%'
                            }
                        },
                        {
                            '$Document.name$': {
                                $iLike: '%' + query + '%'
                            }
                        }
                    ]
                },
                include: [{all: true},
                    {model: models.Document, include: models.File, as: 'Document'}]

            })]).spread(function (companies, transactions) {
            response.successFuncS(res, {"companies": companies, "transactions": transactions});
        });
    } else {
        response.errorFunc(res, consts.CODE.BAD_REQUEST, consts.QUERY_NOT_FOUND);
    }

};

module.exports.searchTransactions = function (req, res) {
    if (req.query.q) {
        var query = req.query.q;
        models.Transaction.findAll({
            where: {
                $or: [
                    {
                        addressBlock: {
                            $iLike: '%' + query + '%'
                        }
                    },
                    {
                        '$Document.name$': {
                            $iLike: '%' + query + '%'
                        }
                    }
                ]
            },
            include: [{all: true},
                {model: models.Document, include: models.File, as: 'Document'}]

        }).then(function (transactions) {
            response.successFuncS(res, transactions);
        })
    }
    else {
        response.errorFunc(res, consts.CODE.BAD_REQUEST, consts.QUERY_NOT_FOUND);
    }


};


