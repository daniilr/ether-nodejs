var models = require('../models');
var response = require('./index');
var consts = require('../utils/consts');

module.exports.updateCompany = function (req, res) {
    var companyId = req.params.companyId ? req.params.companyId : req.user.company;
    models.Company.findOne(
        {
            where: {id: companyId},
            include: [
                {model: models.Logo},
                {model: models.Wallet}
            ]
        }
    ).then(function (company) {
        if (company) {
            company.updateAttributes({
                name: req.body.name, // OOO Рога и Копыта
                country: req.body.country, //Убрать По-умолчанию россия
                data: req.body.data
                //Добавить Печать организации
            }).then(function () {
                response.successFuncF(res, consts.COMPANY_UPDATE, null);
            });

        } else {
            response.errorFunc(res, consts.CODE.NOT_FOUND, consts.COMPANY_NOT_FOUND);
        }
    }, function (err) {
        response.errDbFunc(res, err);
    });

};

module.exports.getCompany = function (req, res) {
    var companyId = req.params.companyId ? req.params.companyId : req.user.company;
    models.Company.findOne(
        {
            where: {id: companyId},
            include: [
                {model: models.Logo},
                {model: models.Wallet}
            ]
        }
    ).then(function (company) {
        if (company) {
            response.successFuncS(res, company);
        } else {
            response.errorFunc(res, consts.CODE.NOT_FOUND, consts.COMPANY_NOT_FOUND);
        }
    }, function (err) {
        response.errDbFunc(res, err);
    });

};
module.exports.searchCompany = function (req, res) {
    if (req.query.q) {
        models.Company.findAll(
            {
                where: {
                    $or: [{
                        name: {$like: '%' + req.query.q + '%'}
                    },
                        {
                            '$Wallet.address$': req.query.q
                        }
                    ]
                },
                include: [
                    {model: models.Wallet, as: 'Wallet'},
                    {model: models.Logo}
                ]
            }
        ).then(function (companies) {
            if (companies && companies.length > 0) {
                response.successFuncS(res, companies);
            } else {
                response.errorFunc(res, consts.CODE.NOT_FOUND, consts.COMPANIES_NOT_FOUND);
            }
        }, function (err) {
            response.errDbFunc(res, err);
        });
    } else {
        response.errorFunc(res, consts.CODE.BAD_REQUEST, consts.QUERY_NOT_FOUND);
    }
};
module.exports.listCompany = function (req, res) {
    models.Company.findAll(
        {
            include: [
                {model: models.Logo},
                {model: models.Wallet}
            ]
        }
    ).then(function (companies) {
        if (companies && companies.length > 0) {
            response.successFuncS(res, companies);
        } else {
            response.errorFunc(res, consts.CODE.NOT_FOUND, consts.COMPANIES_NOT_FOUND);
        }
    }, function (err) {
        response.errDbFunc(res, err);
    });

};