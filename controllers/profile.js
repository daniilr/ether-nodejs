var models = require('../models');
var response = require('./index');
var consts = require('../utils/consts');

module.exports.getProfile = function (req, res) {
    var userId = req.params.id ? req.params.id : req.user.id;
    models.User.findOne(
        {
            where: {id: userId},
            include: [
                {model: models.Profile, include: models.Photo},
                {model: models.Company}
            ]
        }
    ).then(function (user) {
        if (user) {
            response.successFuncS(res, user);
        } else {
            response.errorFunc(res, consts.CODE.NOT_FOUND, consts.PROFILE_NOT_FOUND);
        }
    }, function (err) {
        response.errDbFunc(res, err);
    });
};

module.exports.updateProfile = function (req, res) {
    var userId = req.params.id ? req.params.id : req.user.id;
    models.User.findOne(
        {
            where: {id: userId},
            include: {model: models.Profile}
        }
    ).then(function (user) {
        if (user) {
            user.getProfile().then(function (profile) {
                profile.updateAttributes(
                    {
                        firstname: req.body.firstname,
                        lastname: req.body.lastname,
                        middlename: req.body.middlename
                    }).then(function () {
                    response.successFuncF(res, consts.PROFILE_UPDATE, null);
                });
            })
        } else {
            response.errorFunc(res, consts.CODE.NOT_FOUND, consts.PROFILE_NOT_FOUND);
        }
    }, function (err) {
        response.errDbFunc(res, err);
    });
};