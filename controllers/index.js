module.exports.errDbFunc = function (res,err) {
    res.status(400).json({"status": false, "msg": null, "errors": err.errors});
};

module.exports.errorFunc = function (res,status, msg) {
    res.status(status).json({
        "status": false,
        "msg": msg,
        "errors": [],
        "data": null
    });
};

module.exports.successFuncF = function (res,msg, data) {
    res.status(200).json({"status": true, "msg": msg, "errors": [], data: data});
};

module.exports.successFuncS = function (res,data) {
    res.status(200).json({"status": true, "msg": "", "errors": [], data: data});
};