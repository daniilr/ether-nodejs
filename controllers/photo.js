var models = require('../models');
var diskStorage = require('../utils/imageStorage');
var consts = require('../utils/consts');
var response = require('./index');

module.exports.storagePhoto = diskStorage({
    destination: function (req, file, cb) {
        cb(null, './public/uploads/photo/original');
    }
});

module.exports.deletePhoto = function (req, res) {
    if (req.params.photoId) {
        models.Photo.destroy({where: {id: req.params.photoId, ProfileId: req.user.profile}}).then(function () {
            response.successFuncF(res, consts.PHOTO_DELETED, null);
        }, function (err) {
            response.errDbFunc(res, err);
        })
    } else {
        response.errorFunc(res,consts.CODE.BAD_REQUEST, consts.PHOTO_ID_NOT_FOUND);
    }
};

module.exports.uploadPhoto = function (req, res) {
    var dest =  req.file.path.replace('public','');
    //TODO Add thumbnail images.
    models.Photo.create({
        hash: dest,
        original: dest,
        small: dest,
        normal: dest,
        big: dest
    }).then(function (photo) {
        models.Profile.findOne({
            where: {id: req.user.profile}
        }).then(function (profile) {
            profile.addPhoto(photo).then(function () {
                response.successFuncS(res, photo);
            })
        });
    }, function (err) {
        response.errDbFunc(res, err);
    });
};

module.exports.listPhoto = function (req, res) {
    var profileId = req.query.profile_id ? req.query.profile_id : req.user.profile;
    models.Photo.findAll({where: {ProfileId: profileId}}).then(function (photos) {
        if (photos && photos.length > 0) {
            response.successFuncS(res, photos);
        } else {
            response.errorFunc(res, consts.CODE.NOT_FOUND, consts.PHOTOS_NOT_FOUND);
        }
    }, function (err) {
        response.errDbFunc(res, err);
    });
};