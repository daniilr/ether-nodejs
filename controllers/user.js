var models = require('../models');
var env = process.env.NODE_ENV || 'development';
var config = require('../config/config.json')[env];
var crypto = require('crypto');
var jwt = require('jsonwebtoken');
var utils = require('../utils/utils');
var Sequelize = require("sequelize");
var consts = require('../utils/consts');
var response = require('./index');


module.exports.resetPasswordUser = function (req, res) {
    if (req.query.email) {
        models.User.findOne({where: {email: req.query.email}}).then(function (user) {
                if (user) {
                    var password = utils.generatePass();
                    user.updateAttributes({password: password}).then(function () {
                        var mailClient = req.app.get('mailClient');
                        mailClient.sendEmail({
                            "From": config.postmarkEmail,
                            "To": req.query.email,
                            "Subject": consts.EMAIL_RESET_TITLE,
                            "TextBody": consts.EMAIL_RESET_CONTENT + password
                        });
                        console.log("new password:", password);
                        response.successFuncF(res, consts.USER_PASSWORD_CHANGED, null);
                    });
                } else {
                    response.errorFunc(res, consts.CODE.NOT_FOUND, consts.USER_NOT_FOUND);
                }
            },
            function (err) {
                response.errDbFunc(res, err);
            })
    } else {
        response.errorFunc(res, consts.CODE.BAD_REQUEST, consts.QUERY_NOT_FOUND);
    }
};

module.exports.registerUser = function (req, res) {
    var confirmKey = crypto.randomBytes(config.confirmKeySize).toString('hex');
    var confirmUrl = req.protocol + '://' + req.get('host') + '/confirm?key=' + confirmKey;
    models.User.create({
        email: req.body.email,
        verified: true,
        password: req.body.password,
        confirm: confirmKey
    }).then(function (user) {
        Sequelize.Promise.all([
            models.Company.create({
                name: req.body.company.name,
                data: req.body.company.data
            }),
            models.Profile.create()])
            .spread(function (company, profile) {
                Sequelize.Promise.all([
                    user.setCompany(company),
                    user.setProfile(profile)
                ]).spread(function () {
                        var web3 = req.app.get('web3');
                        var pass = utils.generateWalletPass();
                        web3.personal.newAccount(pass, function (error, address) {
                            if (!error) {
                                models.Wallet.create({
                                    count_local: 10,
                                    balance: 0,
                                    pass: pass,
                                    address: address
                                }).then(function (wallet) {
                                        company.setWallet(wallet).then(function () {
                                            var data = {
                                                id: user.id,
                                                email: user.email,
                                                profile: profile.id,
                                                company: company.id
                                            };
                                            console.log(data);
                                            var mailClient = req.app.get('mailClient');
                                            mailClient.sendEmail({
                                                "From": config.postmarkEmail,
                                                "To": req.body.email,
                                                "Subject": consts.EMAIL_REGISTER_TITLE,
                                                "TextBody": consts.EMAIL_REGISTER_CONTENT + req.body.email + "\n" + req.body.password
                                            });
                                            var token = jwt.sign(data, config.secret, {expiresIn: '365d'});
                                            response.successFuncF(res, consts.USER_HAS_AUTH, {token: token});
                                        });
                                    },
                                    function (err) {
                                        response.errDbFunc(res, err);
                                    });
                            } else {
                                console.log("error create wallet");
                            }
                        });


                    },
                    function (err) {
                        response.errDbFunc(res, err);
                    }
                );
            });
    });
};




module.exports.inviteUser = function (req, res) {
    var confirmKey = crypto.randomBytes(config.confirmKeySize).toString('hex');
    var password = utils.generatePass();
    var inviteUrl = req.protocol + '://' + req.get('host') + '/invite?key=' + confirmKey;
    models.User.create({
        email: req.body.email,
        verified: false,
        password: password,
        confirm: confirmKey
    }).then(function (user) {
        Sequelize.Promise.all([
            models.Company.create({
                name: req.body.company.name,
                data: req.body.company.data
            }),
            models.Profile.create()])
            .spread(function (company, profile) {
                Sequelize.Promise.all([
                    user.setCompany(company),
                    user.setProfile(profile)
                ]).spread(function () {
                        var web3 = req.app.get('web3');
                        var pass = utils.generateWalletPass();
                        web3.personal.newAccount(pass, function (error, address) {
                            if (!error) {
                                models.Wallet.create({
                                    count_local: 10,
                                    balance: 0,
                                    pass: pass,
                                    address: address
                                }).then(function (wallet) {
                                        company.setWallet(wallet).then(function () {
                                            var data = {
                                                id: user.id,
                                                email: user.email,
                                                profile: profile.id,
                                                company: company.id
                                            };
                                            console.log(data);
                                            var mailClient = req.app.get('mailClient');
                                            mailClient.sendEmail({
                                                "From": config.postmarkEmail,
                                                "To": req.body.email,
                                                "Subject": consts.EMAIL_INVITE_TITLE,
                                                "TextBody": consts.EMAIL_INVITE_CONTENT + inviteUrl
                                            });
                                            response.successFuncF(res, company);
                                        });
                                    },
                                    function (err) {
                                        response.errDbFunc(res, err);
                                    });
                            } else {
                                console.log("error create wallet");
                            }
                        });


                    },
                    function (err) {
                        response.errDbFunc(res, err);
                    }
                );
            });
    });
};



module.exports.confirmInviteUser = function (req, res) {
    if (req.query.key) {
        models.User.findOne({where: {confirm: req.query.key}})
            .then(function (user) {
                    if (user) {
                        if (user.verified) {
                            response.errorFunc(res, consts.CODE.CONFLICT, consts.USER_ALREADY_VERIFIED);
                        } else {
                            var password = utils.generatePass();
                            user.updateAttributes({
                                verified: true,
                                password: password}).then(function () {
                                var mailClient = req.app.get('mailClient');
                                mailClient.sendEmail({
                                    "From": config.postmarkEmail,
                                    "To": user.email,
                                    "Subject": consts.EMAIL_RESET_TITLE,
                                    "TextBody": consts.EMAIL_RESET_CONTENT + user.email + "\n" + password
                                });
                                var token = jwt.sign(data, config.secret, {expiresIn: '365d'});
                                response.successFuncF(res, consts.USER_HAS_AUTH, {token: token});
                            });
                        }
                    } else {
                        response.errorFunc(res, consts.CODE.NOT_FOUND, consts.USER_NOT_FOUND);
                    }
                },
                function (err) {
                    response.errDbFunc(res, err);
                });
    } else {
        response.errorFunc(res, consts.CODE.BAD_REQUEST, consts.QUERY_NOT_FOUND);
    }


};

module.exports.confirmUser = function (req, res) {
    if (req.query.key) {
        models.User.findOne({where: {confirm: req.query.key}})
            .then(function (user) {
                    if (user) {
                        if (user.verified) {
                            response.errorFunc(res, consts.CODE.CONFLICT, consts.USER_ALREADY_VERIFIED);
                        } else {
                            user.updateAttributes({verified: true}).then(function () {
                                response.successFuncF(res, consts.USER_CONFIRMED, null);
                            });
                        }
                    } else {
                        response.errorFunc(res, consts.CODE.NOT_FOUND, consts.USER_NOT_FOUND);
                    }
                },
                function (err) {
                    response.errDbFunc(res, err);
                });
    } else {
        response.errorFunc(res, consts.CODE.BAD_REQUEST, consts.QUERY_NOT_FOUND);
    }


};

module.exports.logoutUser = function (req, res) {
    //TODO Realize method for delete user token
    response.successFuncF(res, consts.USER_LOGOUT, null);
};

module.exports.loginUser = function (req, res) {
    models.User.findOne({
        where: {
            email: req.body.email
        }, include: [{
            model: models.Profile
        }, {
            model: models.Company
        }]
    }).then(function (user) {
            if (user) {
                if (user.verified && !user.blocked && !user.deleted) {
                    if (user.validPassword(req.body.password)) {
                        var data = {
                            id: user.id,
                            email: user.email,
                            profile: user.Profile.id,
                            company: user.Company.id
                        };
                        console.log(data);
                        var mailClient = req.app.get('mailClient');
                        mailClient.sendEmail({
                            "From": config.postmarkEmail,
                            "To": req.query.email,
                            "Subject": consts.EMAIL_LOGIN_TITLE,
                            "TextBody": consts.EMAIL_LOGIN_CONTENT
                        });
                        var token = jwt.sign(data, config.secret, {expiresIn: '365d'});
                        response.successFuncF(res, consts.USER_HAS_AUTH, {token: token});

                    }
                    else {
                        response.errorFunc(res, consts.CODE.UNAUTHORIZED, consts.INCORRECT_LOGIN_PASSWORD);
                    }
                } else if (!user.verified) {
                    response.errorFunc(res, consts.CODE.FORBIDDEN, consts.CONFIRM_ACCOUNT);
                } else if (user.blocked) {
                    response.errorFunc(res, consts.CODE.LOCKED, consts.USER_BANNED);

                } else if (user.deleted) {
                    response.errorFunc(res, consts.CODE.GONE, consts.USER_DELETED);
                }
                else {
                    response.errorFunc(res, consts.CODE.UNAUTHORIZED, consts.INCORRECT_LOGIN_PASSWORD);
                }
            } else {
                response.errorFunc(res, consts.CODE.UNAUTHORIZED, consts.INCORRECT_LOGIN_PASSWORD);
            }
        },
        function (err) {
            response.errDbFunc(res, err);
        }
    )
    ;
}