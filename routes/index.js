var express = require('express');
var router = express.Router();
var models = require('../models');
var env = process.env.NODE_ENV || 'development';

/*
 * Home public page
 * */

router.get('/', function (req, res, next) {
    res.render('index', {title: 'Express'});
});


module.exports = router;
