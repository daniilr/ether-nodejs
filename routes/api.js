var express = require('express');
var router = express.Router();
var multerObj = require('multer');
var models = require('../models');
var env = process.env.NODE_ENV || 'development';
var config = require('../config/config.json')[env];
var photoController = require('../controllers/photo');
var companyController = require('../controllers/company');
var logoController = require('../controllers/logo');
var profileController = require('../controllers/profile');
var transactionController = require('../controllers/transaction');
var userController = require('../controllers/user');
var utils = require("../utils/utils.js");

/**
 * @api {post} /invite Invite user
 * @apiName InviteUser
 * @apiGroup User
 *
 * @apiParam {String} email  e-mail corp.
 * @apiParam {String} Company name
 * @apiParam {Object} Company data
 *
 * @apiParamExample {json} Request-Example:
 *     {
 *      "email": "ivan@admin.com"
 *      "company": {
 *              "name": "OOO Ivanovich"
 *              "data": {
 *                  "inn": 1234567890
 *              }
 *          }
 *     }
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *    {"status": true, "msg": "Invite was sent!", "errors": [], data: company}
 */


router.route('/invite')
    .post(userController.inviteUser);


/**
 * @api {post} /register Register user
 * @apiName RegisterUser
 * @apiGroup User
 *
 * @apiParam {String} email  e-mail corp.
 * @apiParam {String} password password account
 * @apiParam {String} Company name
 * @apiParam {Object} Company data
 *
 * @apiParamExample {json} Request-Example:
 *     {
 *      "email": "ivan@admin.com"
 *      "password": "Ivanovich123"
 *      "company": {
 *              "name": "OOO Ivanovich"
 *              "data": {
 *                  "inn": 1234567890
 *              }
 *          }
 *     }
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *    {"status": true, "msg": "Please confirm your account!", "errors": [], data: {confirm: "http://...."}}
 */


router.route('/register')
    .post(userController.registerUser);


/**
 * @api {get} /reset Reset password
 * @apiName ResetPasswordAccount
 * @apiGroup User
 *
 * @apiParam {String} email  e-mail corp.
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 */

router.route('/reset')
    .get(userController.resetPasswordUser);


/**
 * @api {get} /invite Confirm invite account
 * @apiName confirmInviteUser
 * @apiGroup User
 *
 * @apiParam {String} key verification key.
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *  {
 *  "msg": "User has authorized!"
 *  "errors": [0]
 *  "data": {
 *  "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiZW1haWwiOiJpdmFuQGFkbWluLmNvbSIsInByb2ZpbGUiOjEsImNvbXBhbnkiOjEsImlhdCI6MTQ3NTI2MDc1NiwiZXhwIjoxNDc1MjYyMTk2fQ.EHJESJl6Hiel2wXbiduQwC8mptOs1ocRm3IIasaVQrk"
 *      }
 *  }
 */

router.route('/invite')
    .get(userController.confirmInviteUser);


/**
 * @api {get} /confirm Confirm account
 * @apiName ConfirmAccount
 * @apiGroup User
 *
 * @apiParam {String} key verification key.
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *    {
 *    "status": true
 *    "msg": "User has been confirmed!"
 *    "errors": [0]
 *    "data": null
 *    }
 */

router.route('/confirm')
    .get(userController.confirmUser);

/**
 * Log out
 * GET /logout
 */


/**
 * @api {get} /logout  Logout account
 * @apiName LogoutUser
 * @apiDescription Method in progress..
 * @apiGroup User
 *
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *
 */

router.route('/logout')
    .get(userController.logoutUser);

/**
 * @api {post} /login Login user
 * @apiName LoginUser
 * @apiGroup User
 *
 * @apiParam {String} email  e-mail corp.
 * @apiParam {String} password password account
 *
 * @apiParamExample {json} Request-Example:
 *     {
 *      "email": "ivan@admin.com"
 *      "password": "Ivanovich123"
 *     }
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *
 *  {
 *  "status": true
 *  "msg": "User has authorized!"
 *  "errors": [0]
 *  "data": {
 *  "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiZW1haWwiOiJpdmFuQGFkbWluLmNvbSIsInByb2ZpbGUiOjEsImNvbXBhbnkiOjEsImlhdCI6MTQ3NTI2MDc1NiwiZXhwIjoxNDc1MjYyMTk2fQ.EHJESJl6Hiel2wXbiduQwC8mptOs1ocRm3IIasaVQrk"
 *      }
 *  }
 *
 */
router.route('/login')
    .post(userController.loginUser);


/**
 * @api {get} /user Request User profile
 * @apiName GetUserProfile
 * @apiGroup User
 *
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *    {"status": true, "msg": "", "errors": [], data: User}
 */
router.route('/user/')
    .get(profileController.getProfile);


/**
 * @api {get} /user/:id Request User profile
 * @apiName GetUserProfile
 * @apiGroup User
 *
 * @apiParam {Number} [id] Users unique ID.
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *    {"status": true, "msg": "", "errors": [], data: User}
 */
router.route('/user/:id([0-9]+)')
    .get(profileController.getProfile);

/**
 * @api {put} /profile/ Update current user profile
 * @apiName UpdateCurrentUserProfile
 * @apiGroup User
 *
 * @apiParam {String} firstname   Firstname of the User.
 * @apiParam {String} lastname     Lastname of the User.
 * @apiParam {String} middlename   Middlename of the User.
 *
 * @apiParamExample {json} Request-Example:
 *     {
 *      "firstname": "Ivan"
 *      "lastname": "Ivanov",
 *      "middlename": "Ivanovich"
 *     }
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *    {"status": true, "msg": "Profile has been updated!", "errors": [], data: null}
 */
router.route('/profile')
    .put(profileController.updateProfile);


/**
 * @api {put} /company/ Update current company
 * @apiName UpdateCurrentUserCompany
 * @apiGroup Company
 *
 * @apiParam {String} name   Name company.
 * @apiParam {String} data Additional data company.
 * @apiParam {Integer} country   Country of the Company.
 *
 * @apiParamExample {json} Request-Example:
 *      {
 *      "name": "ООО Рога и копыта"
 *      "data": {"INN": 123456789}
 *      "country": 7
 *     }
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *    {"status": true, "msg": "Company has been updated!", "errors": [], data: null}
 */
router.route('/company')
    .put(companyController.updateCompany);


/**
 * @api {get} /company Request Company
 * @apiName GetCompany
 * @apiGroup Company
 *
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *    {"status": true, "msg": "", "errors": [], data: Company}
 */
router.route('/company')
    .get(companyController.getCompany);


/**
 * @api {get} /company/:id Request Company
 * @apiName GetCompany
 * @apiGroup Company
 *
 * @apiParam {Number} [id] Companies unique ID.
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *    {"status": true, "msg": "", "errors": [], data: Company}
 */
router.route('/company/:companyId([0-9]+)')
    .get(companyController.getCompany);

/**
 * @api {get} /company/list Request Companies list
 * @apiName GetCompanyList
 * @apiGroup Company
 *
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *    {"status": true, "msg": "", "errors": [], data: Company[]}
 */
router.route('/company/list')
    .get(companyController.listCompany);

/**
 * @api {get} /company/search Request Company search
 * @apiName SearchCompany
 * @apiGroup Company
 *
 * @apiParam {String} q query.
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *    {"status": true, "msg": "", "errors": [], data: Company[]}
 */
router.route('/company/search')
    .get(companyController.searchCompany);


/**
 * @api {post} /company/logo Upload logo current company
 * @apiName UploadLogoCompany
 * @apiGroup Company
 *
 * @apiParam {File} logo Logo image file.
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *    {"status": true, "msg": "", "errors": [], data: Logo}
 */
router.route('/company/logo')
    .post(multerObj({storage: logoController.storageLogo})
        .single('logo'), logoController.uploadLogo);

/**
 * @api {delete} /company/logo Remove logo current company
 * @apiName RemoveLogoCompany
 * @apiGroup Company
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *    {"status": true, "msg": "Logo has been deleted!", "errors": [], data: null}
 */
router.route('/company/logo')
    .delete(logoController.deleteLogo);

/**
 * @api {delete} /photo/:photoId Remove photo current user
 * @apiName RemovePhotoUser
 * @apiGroup User
 *
 *
 * @apiParam {Number} photoId Photos unique ID .
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *    {"status": true, "msg": "Photo has been deleted!", "errors": [], data: null}
 */
router.route('/photo/:photoId([0-9]+)')
    .delete(photoController.deletePhoto);

/**
 * @api {post} /photo Upload photo current user
 * @apiName UploadPhotoUser
 * @apiGroup User
 *
 * @apiParam {File} photo Photo image file.
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *    {"status": true, "msg": "", "errors": [], data: Photo}
 */
router.route('/photo')
    .post(multerObj({storage: photoController.storagePhoto})
        .single('photo'), photoController.uploadPhoto);

/**
 * @api {get} /photo/list Request Photos list
 * @apiName GetPhotosList
 * @apiGroup User
 *
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *    {"status": true, "msg": "", "errors": [], data: Photo[]}
 */
router.route('/photo/list')
    .get(photoController.listPhoto);


/**
 * @api {post} /file Upload file
 * @apiName UploadFile
 * @apiGroup Transaction
 *
 * @apiParam {File} file uploaded file.
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *    {"status": true, "msg": "", "errors": [], data: File}
 */
router.route('/file')
    .post(multerObj({storage: transactionController.storageDocument})
        .single('file'), transactionController.uploadFile);


/**
 * @api {post} /transaction Create transaction
 * @apiName CreateTransaction
 * @apiGroup Transaction
 *
 * @apiParam {Integer[]} members company of the Transaction.
 * @apiParam {Integer[]} files  of the Document.
 * @apiParam {String} name   name of the Document.
 * @apiParam {String} sign   sign of the Document.
 *
 * @apiParamExample {json} Request-Example:
 *
 *      "members": [1]
 *      "document": {
 *          "files": [1,2,3],
 *          "name": "Contract buy"
 *          "sign": 147525521000
 *          }
 *     }
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *    {"status": true, "msg": "", "errors": [], data: Transaction}
 */

router.route('/transaction')
    .post(transactionController.createTransaction);


/**
 * @api {get} /transaction/:id Request Transaction
 * @apiName GetTransaction
 * @apiGroup Transaction
 *
 * @apiParam {Number} id Transaction unique ID.
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *    {"status": true, "msg": "", "errors": [], data: Transaction}
 */

router.route('/transaction/:transactionId([0-9]+)')
    .get(transactionController.getTransaction);

/**
 * @api {post} /transaction/:id Decision of the Transaction
 * @apiName SetDecisionTransaction
 * @apiGroup Transaction
 *
 * @apiParam {Number} id Transaction unique ID.
 * @apiParam {String} action Action ("approve","decline")
 * @apiParam {String} comment Comment to Action.
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *    {"status": true, "msg": "", "errors": [], data: Transaction}
 */

router.route('/transaction/:transactionId([0-9]+)')
    .post(transactionController.decisionTransaction);

/**
 * @api {post} /transaction/:id Condition of the Transaction
 * @apiName SetConditionTransaction
 * @apiGroup Transaction
 *
 * @apiParam {Number} id Transaction unique ID.
 * @apiParam {String} condition Condition ('active', 'closed','broken')
 * @apiParam {String} comment Comment to Action.
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *    {"status": true, "msg": "", "errors": [], data: Transaction}
 */

router.route('/transaction/:transactionId([0-9]+)/condition')
    .post(transactionController.conditionTransaction);


/**
 * @api {delete} /transaction/:transactionId delete Transaction
 * @apiName DeleteTransaction
 * @apiGroup Transaction
 *
 * @apiParam {Number} id Transaction unique ID.
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *    {"status": true, "msg": "", "errors": [], data: null}
 */


router.route('/transaction/:transactionId([0-9]+)')
    .delete(transactionController.deleteTransaction);


/**
 * @api {get} /company/transaction/list Request current company list transaction
 * @apiName GetCurrentCompanyListTransaction
 * @apiGroup Company
 *
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *    {"status": true, "msg": "", "errors": [], data: Transaction[]}
 */
router.route('/company/transaction/list')
    .get(transactionController.getTransactions);


/**
 * @api {get} /company/:id/transaction/list Request company list transaction
 * @apiName GetCompanyListTransaction
 * @apiGroup Company
 *
 * @apiParam {Number} [companyId] Company unique ID.
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *    {"status": true, "msg": "", "errors": [], data: Transaction[]}
 */
router.route('/company/:companyId([0-9]+)/transaction/list')
    .get(transactionController.getTransactions);


/**
 * @api {get} /company/:companyId([0-9]+)/transaction/search Request Company search
 * @apiName SearchCompany
 * @apiDescription Method in progress..
 * @apiGroup Company
 *
 * @apiParam {Number} [companyId] Company unique ID.
 * @apiParam {String} q query.
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *    {"status": true, "msg": "", "errors": [], data: Transaction[]}
 */
router.route('/company/:companyId([0-9]+)/transaction/search')
    .get(transactionController.searchTransactions);

/**
 * @api {get} /company/transaction/search Request Transaction in current company search
 * @apiName SearchTransactionCurrentCompany
 * @apiGroup Company
 *
 * @apiParam {String} q query.
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *    {"status": true, "msg": "", "errors": [], data: Transaction[]}
 */
router.route('/company/transaction/search')
    .get(transactionController.searchTransactions);


/**
 * @api {get} /search Request global search
 * @apiName GlobalSearch
 * @apiGroup Search
 *
 * @apiParam {String} q query.
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *    {"status": true, "msg": "", "errors": [], data: {companies:Company[], transactions: {Transaction[]}, profiles: {Profile[]}}}
 */
router.route('/search')
    .get(transactionController.globalSearch);


router.route('/core/balance')
    .get(
        function (req, res) {
            var web3 = req.app.get('web3');
            var balance = web3.eth.getBalance("0x48a3f33a95803e869649daed4a215545234b8bdd");
            console.log(balance.toString(10));
        });


router.route('/core/create')
    .get(
        function (req, res) {
            var web3 = req.app.get('web3');
            var pass = utils.generateWalletPass();
            web3.personal.newAccount(pass, function (error, result) {
                    if (!error) {
                        console.log(pass, result);
                    }
                });
        });

router.route('/core/account/list')
    .get(function (req, res) {
        var web3 = req.app.get('web3');
        web3.eth.getAccounts(function (err, result) {
            if (result) {
                console.log(result);
            }
        });
    });

module.exports = router;


