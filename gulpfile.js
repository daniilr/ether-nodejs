jsLibs = [
    "./node_modules/angular/angular.min.js",
    "./node_modules/angular-animate/angular-animate.min.js",
    "./node_modules/angular-aria/angular-aria.min.js",
    "./node_modules/angular-material/angular-material.min.js",
    "./node_modules/angular-messages/angular-messages-min.js"

];
cssFiles = [
    "./node_modules/angular-material/angular-material.min.css"
];

var gulp = require('gulp');
var less = require('gulp-less');
var watch = require('gulp-watch');
var concat = require('gulp-concat');
var concatCss = require('gulp-concat-css');

gulp.task('vendorScripts', function () {
    return gulp.src(jsLibs)
        .pipe(concat('core.js'))
        .pipe(gulp.dest('./public/javascripts'))
});

gulp.task('vendorCSS', function () {
    return gulp.src(cssFiles)
        .pipe(concatCss('style.css'))
        .pipe(gulp.dest('./public/stylesheets'))
});