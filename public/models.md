##Company##

----------
**name**
*type:* STRING
**country**
*type:*  INTEGER
**data**
*type:*  JSONB
**verified**
*type:* BOOLEAN
**Logo**
*type:* Logo
**Wallet**
*type:* Wallet

## Logo ##

----------
**hash**
*type:* STRING
**small**
*type:* STRING
**normal**
*type:* STRING
**big**
*type:* STRING
**original**
*type:* STRING

## Photo ##

----------
**hash**
*type:* STRING
**small**
*type:* STRING
**normal**
*type:* STRING
**big**
*type:* STRING
**original**
*type:* STRING

##Wallet##

----------

**count_local**
*type:* INTEGER
**count_blockchain**
*type:* INTEGER
**balance**
*type:* DOUBLE
**address**
*type:* STRING

##Document##

----------
**name **
*type:* STRING
**sign**
*type:* DATE
**Files**
*type:* File[]

##File##

----------
**uuid**
*type:* STRING,
**filename**
*type:* STRING
**mimetype**
*type:* STRING
**size**
*type:* INTEGER
**hash **
*type:* STRING
**destination **
*type: * STRING
**User**
*type:* User
**Document**
*type:* Document

##Profile##

----------
**firstname**
*type:* STRING
**lastname**
*type:* STRING
**middlename**
*type:* STRING
**Photos**
*type:* Photo[]

##User##

----------
**email**
*type:* STRING
**password**
*type:* STRING
**confirm**
*type:* STRING
**blocked**
*type:* BOOLEAN
**deleted**
*type:* BOOLEAN
**verified**
*type:* BOOLEAN
**Profile**
*type:* Profile
**Company**
*type:* Company

##Transaction##

----------
**status**
*type:* ENUM('approve', 'decline', 'pending')
**condition**
*type:* ENUM('active', 'closed','broken')
**type**
*type:* ENUM('local', 'blockchain')
**Document**
*type:* Document
**OwnerTransaction**
*type:* Company
**addressBlock**
*type:* STRING
**Decisions**
*type:* Decision[]
**Conditions**
*type:* Condition[]
**Members**
*type:* Company[]

##Decision##

----------
**action**
*type:* ENUM('approve', 'decline', 'pending')
**comment**
*type:* STRING
**Company**
*type:* Company

##Condition##

----------
**action**
*type:* ENUM('active', 'closed','broken')
**comment**
*type:* STRING
**Company**
*type:* Company