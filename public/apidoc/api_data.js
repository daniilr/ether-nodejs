define({ "api": [
  {
    "type": "get",
    "url": "/company",
    "title": "Request Company",
    "name": "GetCompany",
    "group": "Company",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": " HTTP/1.1 200 OK\n{\"status\": true, \"msg\": \"\", \"errors\": [], data: Company}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/api.js",
    "groupTitle": "Company"
  },
  {
    "type": "get",
    "url": "/company/:id",
    "title": "Request Company",
    "name": "GetCompany",
    "group": "Company",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "id",
            "description": "<p>Companies unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": " HTTP/1.1 200 OK\n{\"status\": true, \"msg\": \"\", \"errors\": [], data: Company}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/api.js",
    "groupTitle": "Company"
  },
  {
    "type": "get",
    "url": "/company/list",
    "title": "Request Companies list",
    "name": "GetCompanyList",
    "group": "Company",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": " HTTP/1.1 200 OK\n{\"status\": true, \"msg\": \"\", \"errors\": [], data: Company[]}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/api.js",
    "groupTitle": "Company"
  },
  {
    "type": "get",
    "url": "/company/:id/transaction/list",
    "title": "Request company list transaction",
    "name": "GetCompanyListTransaction",
    "group": "Company",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "companyId",
            "description": "<p>Company unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": " HTTP/1.1 200 OK\n{\"status\": true, \"msg\": \"\", \"errors\": [], data: Transaction[]}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/api.js",
    "groupTitle": "Company"
  },
  {
    "type": "get",
    "url": "/company/transaction/list",
    "title": "Request current company list transaction",
    "name": "GetCurrentCompanyListTransaction",
    "group": "Company",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": " HTTP/1.1 200 OK\n{\"status\": true, \"msg\": \"\", \"errors\": [], data: Transaction[]}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/api.js",
    "groupTitle": "Company"
  },
  {
    "type": "delete",
    "url": "/company/logo",
    "title": "Remove logo current company",
    "name": "RemoveLogoCompany",
    "group": "Company",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": " HTTP/1.1 200 OK\n{\"status\": true, \"msg\": \"Logo has been deleted!\", \"errors\": [], data: null}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/api.js",
    "groupTitle": "Company"
  },
  {
    "type": "get",
    "url": "/company/search",
    "title": "Request Company search",
    "name": "SearchCompany",
    "group": "Company",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "q",
            "description": "<p>query.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": " HTTP/1.1 200 OK\n{\"status\": true, \"msg\": \"\", \"errors\": [], data: Company[]}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/api.js",
    "groupTitle": "Company"
  },
  {
    "type": "get",
    "url": "/company/:companyId([0-9]+)/transaction/search",
    "title": "Request Company search",
    "name": "SearchCompany",
    "description": "<p>Method in progress..</p>",
    "group": "Company",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "companyId",
            "description": "<p>Company unique ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "q",
            "description": "<p>query.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": " HTTP/1.1 200 OK\n{\"status\": true, \"msg\": \"\", \"errors\": [], data: Transaction[]}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/api.js",
    "groupTitle": "Company"
  },
  {
    "type": "get",
    "url": "/company/transaction/search",
    "title": "Request Transaction in current company search",
    "name": "SearchTransactionCurrentCompany",
    "group": "Company",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "q",
            "description": "<p>query.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": " HTTP/1.1 200 OK\n{\"status\": true, \"msg\": \"\", \"errors\": [], data: Transaction[]}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/api.js",
    "groupTitle": "Company"
  },
  {
    "type": "put",
    "url": "/company/",
    "title": "Update current company",
    "name": "UpdateCurrentUserCompany",
    "group": "Company",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Name company.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "data",
            "description": "<p>Additional data company.</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "country",
            "description": "<p>Country of the Company.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": " {\n \"name\": \"ООО Рога и копыта\"\n \"data\": {\"INN\": 123456789}\n \"country\": 7\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": " HTTP/1.1 200 OK\n{\"status\": true, \"msg\": \"Company has been updated!\", \"errors\": [], data: null}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/api.js",
    "groupTitle": "Company"
  },
  {
    "type": "post",
    "url": "/company/logo",
    "title": "Upload logo current company",
    "name": "UploadLogoCompany",
    "group": "Company",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "File",
            "optional": false,
            "field": "logo",
            "description": "<p>Logo image file.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": " HTTP/1.1 200 OK\n{\"status\": true, \"msg\": \"\", \"errors\": [], data: Logo}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/api.js",
    "groupTitle": "Company"
  },
  {
    "type": "get",
    "url": "/search",
    "title": "Request global search",
    "name": "GlobalSearch",
    "group": "Search",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "q",
            "description": "<p>query.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": " HTTP/1.1 200 OK\n{\"status\": true, \"msg\": \"\", \"errors\": [], data: {companies:Company[], transactions: {Transaction[]}, profiles: {Profile[]}}}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/api.js",
    "groupTitle": "Search"
  },
  {
    "type": "post",
    "url": "/transaction",
    "title": "Create transaction",
    "name": "CreateTransaction",
    "group": "Transaction",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer[]",
            "optional": false,
            "field": "members",
            "description": "<p>company of the Transaction.</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer[]",
            "optional": false,
            "field": "files",
            "description": "<p>of the Document.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>name of the Document.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "sign",
            "description": "<p>sign of the Document.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "\n \"members\": [1]\n \"document\": {\n     \"files\": [1,2,3],\n     \"name\": \"Contract buy\"\n     \"sign\": 147525521000\n     }\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": " HTTP/1.1 200 OK\n{\"status\": true, \"msg\": \"\", \"errors\": [], data: Transaction}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/api.js",
    "groupTitle": "Transaction"
  },
  {
    "type": "delete",
    "url": "/transaction/:transactionId",
    "title": "delete Transaction",
    "name": "DeleteTransaction",
    "group": "Transaction",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Transaction unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": " HTTP/1.1 200 OK\n{\"status\": true, \"msg\": \"\", \"errors\": [], data: null}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/api.js",
    "groupTitle": "Transaction"
  },
  {
    "type": "get",
    "url": "/transaction/:id",
    "title": "Request Transaction",
    "name": "GetTransaction",
    "group": "Transaction",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Transaction unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": " HTTP/1.1 200 OK\n{\"status\": true, \"msg\": \"\", \"errors\": [], data: Transaction}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/api.js",
    "groupTitle": "Transaction"
  },
  {
    "type": "post",
    "url": "/transaction/:id",
    "title": "Condition of the Transaction",
    "name": "SetConditionTransaction",
    "group": "Transaction",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Transaction unique ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "condition",
            "description": "<p>Condition ('active', 'closed','broken')</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "comment",
            "description": "<p>Comment to Action.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": " HTTP/1.1 200 OK\n{\"status\": true, \"msg\": \"\", \"errors\": [], data: Transaction}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/api.js",
    "groupTitle": "Transaction"
  },
  {
    "type": "post",
    "url": "/transaction/:id",
    "title": "Decision of the Transaction",
    "name": "SetDecisionTransaction",
    "group": "Transaction",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Transaction unique ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "action",
            "description": "<p>Action (&quot;approve&quot;,&quot;decline&quot;)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "comment",
            "description": "<p>Comment to Action.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": " HTTP/1.1 200 OK\n{\"status\": true, \"msg\": \"\", \"errors\": [], data: Transaction}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/api.js",
    "groupTitle": "Transaction"
  },
  {
    "type": "post",
    "url": "/file",
    "title": "Upload file",
    "name": "UploadFile",
    "group": "Transaction",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "File",
            "optional": false,
            "field": "file",
            "description": "<p>uploaded file.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": " HTTP/1.1 200 OK\n{\"status\": true, \"msg\": \"\", \"errors\": [], data: File}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/api.js",
    "groupTitle": "Transaction"
  },
  {
    "type": "get",
    "url": "/confirm",
    "title": "Confirm account",
    "name": "ConfirmAccount",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "key",
            "description": "<p>verification key.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": " HTTP/1.1 200 OK\n{\n\"status\": true\n\"msg\": \"User has been confirmed!\"\n\"errors\": [0]\n\"data\": null\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/api.js",
    "groupTitle": "User"
  },
  {
    "type": "get",
    "url": "/photo/list",
    "title": "Request Photos list",
    "name": "GetPhotosList",
    "group": "User",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": " HTTP/1.1 200 OK\n{\"status\": true, \"msg\": \"\", \"errors\": [], data: Photo[]}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/api.js",
    "groupTitle": "User"
  },
  {
    "type": "get",
    "url": "/user",
    "title": "Request User profile",
    "name": "GetUserProfile",
    "group": "User",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": " HTTP/1.1 200 OK\n{\"status\": true, \"msg\": \"\", \"errors\": [], data: User}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/api.js",
    "groupTitle": "User"
  },
  {
    "type": "get",
    "url": "/user/:id",
    "title": "Request User profile",
    "name": "GetUserProfile",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "id",
            "description": "<p>Users unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": " HTTP/1.1 200 OK\n{\"status\": true, \"msg\": \"\", \"errors\": [], data: User}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/api.js",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "/invite",
    "title": "Invite user",
    "name": "InviteUser",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>e-mail corp.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Company",
            "description": "<p>name</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n \"email\": \"ivan@admin.com\"\n \"company\": {\n         \"name\": \"OOO Ivanovich\"\n         \"data\": {\n             \"inn\": 1234567890\n         }\n     }\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": " HTTP/1.1 200 OK\n{\"status\": true, \"msg\": \"Invite was sent!\", \"errors\": [], data: company}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/api.js",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "/login",
    "title": "Login user",
    "name": "LoginUser",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>e-mail corp.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>password account</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n \"email\": \"ivan@admin.com\"\n \"password\": \"Ivanovich123\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "   HTTP/1.1 200 OK\n\n{\n\"status\": true\n\"msg\": \"User has authorized!\"\n\"errors\": [0]\n\"data\": {\n\"token\": \"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiZW1haWwiOiJpdmFuQGFkbWluLmNvbSIsInByb2ZpbGUiOjEsImNvbXBhbnkiOjEsImlhdCI6MTQ3NTI2MDc1NiwiZXhwIjoxNDc1MjYyMTk2fQ.EHJESJl6Hiel2wXbiduQwC8mptOs1ocRm3IIasaVQrk\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/api.js",
    "groupTitle": "User"
  },
  {
    "type": "get",
    "url": "/logout",
    "title": "Logout account",
    "name": "LogoutUser",
    "description": "<p>Method in progress..</p>",
    "group": "User",
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/api.js",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "/register",
    "title": "Register user",
    "name": "RegisterUser",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>e-mail corp.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>password account</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Company",
            "description": "<p>name</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n \"email\": \"ivan@admin.com\"\n \"password\": \"Ivanovich123\"\n \"company\": {\n         \"name\": \"OOO Ivanovich\"\n         \"data\": {\n             \"inn\": 1234567890\n         }\n     }\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": " HTTP/1.1 200 OK\n{\"status\": true, \"msg\": \"Please confirm your account!\", \"errors\": [], data: {confirm: \"http://....\"}}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/api.js",
    "groupTitle": "User"
  },
  {
    "type": "delete",
    "url": "/photo/:photoId",
    "title": "Remove photo current user",
    "name": "RemovePhotoUser",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "photoId",
            "description": "<p>Photos unique ID .</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": " HTTP/1.1 200 OK\n{\"status\": true, \"msg\": \"Photo has been deleted!\", \"errors\": [], data: null}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/api.js",
    "groupTitle": "User"
  },
  {
    "type": "get",
    "url": "/reset",
    "title": "Reset password",
    "name": "ResetPasswordAccount",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>e-mail corp.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/api.js",
    "groupTitle": "User"
  },
  {
    "type": "put",
    "url": "/profile/",
    "title": "Update current user profile",
    "name": "UpdateCurrentUserProfile",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "firstname",
            "description": "<p>Firstname of the User.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lastname",
            "description": "<p>Lastname of the User.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "middlename",
            "description": "<p>Middlename of the User.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n \"firstname\": \"Ivan\"\n \"lastname\": \"Ivanov\",\n \"middlename\": \"Ivanovich\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": " HTTP/1.1 200 OK\n{\"status\": true, \"msg\": \"Profile has been updated!\", \"errors\": [], data: null}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/api.js",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "/photo",
    "title": "Upload photo current user",
    "name": "UploadPhotoUser",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "File",
            "optional": false,
            "field": "photo",
            "description": "<p>Photo image file.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": " HTTP/1.1 200 OK\n{\"status\": true, \"msg\": \"\", \"errors\": [], data: Photo}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/api.js",
    "groupTitle": "User"
  },
  {
    "type": "get",
    "url": "/invite",
    "title": "Confirm invite account",
    "name": "confirmInviteUser",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "key",
            "description": "<p>verification key.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "   HTTP/1.1 200 OK\n{\n\"msg\": \"User has authorized!\"\n\"errors\": [0]\n\"data\": {\n\"token\": \"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiZW1haWwiOiJpdmFuQGFkbWluLmNvbSIsInByb2ZpbGUiOjEsImNvbXBhbnkiOjEsImlhdCI6MTQ3NTI2MDc1NiwiZXhwIjoxNDc1MjYyMTk2fQ.EHJESJl6Hiel2wXbiduQwC8mptOs1ocRm3IIasaVQrk\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "routes/api.js",
    "groupTitle": "User"
  }
] });
