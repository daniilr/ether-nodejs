var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var bodyParser = require('body-parser');
var expressValidator = require('express-validator');
var api = require('./routes/api');
var index = require('./routes/index');
var app = express();
var ejwt = require('express-jwt');
var env = process.env.NODE_ENV || 'development';
var config = require('./config/config.json')[env];
var postmark = require("postmark");
var mailClient = new postmark.Client(config.postmarkApiKey);
var Web3 = require('web3_extended');
var web3 = new Web3.create({
    host: 'http://localhost:8545',
    personal: true,
    admin: true,
    debug: false
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.set('web3', web3);
app.set('mailClient', mailClient);

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(expressValidator());
app.use(express.static(path.join(__dirname, 'public')));

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization, Content-Length, X-Requested-With');

    // intercept OPTIONS method
    if ('OPTIONS' == req.method) {
        res.sendStatus(200);
    }
    else {
        next();
    }
});


/**
 * Public routes
 */

app.use('/', index);

/**
 * Check header Authorization for Api routes
 * Simple
 * Authorization: Bearer eyJhbGc....
 */
var ejwt_route = ejwt({secret: config.secret});

app.use('/profile', ejwt_route);
app.use('/user', ejwt_route);
app.use('/company', ejwt_route);
app.use('/photo', ejwt_route);
app.use('/file', ejwt_route);
app.use('/transaction', ejwt_route);
app.use('/search', ejwt_route);
app.use('/invite', ejwt_route);
app.use('/core', ejwt_route);



app.use(function (err, req, res, next) {
    if (err.constructor.name === 'UnauthorizedError') {
        res.status(401).json({
            "status": false,
            "msg": "Unauthorized user",
            "errors": []
        });
    }
});

/**
 * Api routes
 */

app.use('/', api);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

module.exports = web3;
module.exports = app;
