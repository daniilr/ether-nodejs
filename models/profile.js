/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
    var Profile = sequelize.define('Profile', {
        firstname: {
            type: DataTypes.STRING,
            allowNull: false,
            defaultValue: ""
        },
        lastname: {
            type: DataTypes.STRING,
            allowNull: false,
            defaultValue: ""
        },
        middlename: {
            type: DataTypes.STRING,
            allowNull: false,
            defaultValue: ""
        }

    }, {
        paranoid: true,
        classMethods: {
            associate: function (models) {
                Profile.hasMany(models.Photo)
            }
        }
    });
    return Profile;
};
