/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
    var Transaction = sequelize.define('Transaction', {
        status: {
            type: DataTypes.ENUM('approve', 'decline', 'pending'),
            allowNull: false,
            defaultValue: 'pending'
        },
        condition: {
            type: DataTypes.ENUM('active', 'closed'),
            allowNull: false,
            defaultValue: 'active'
        },
        type: {
            type: DataTypes.ENUM('local', 'blockchain'),
            allowNull: false,
            defaultValue: 'local'
        },
        addressBlock: {
            type: DataTypes.STRING,
            allowNull: false,
            defaultValue: ''
        }

    }, {
        paranoid: true,
        classMethods: {
            associate: function (models) {
                Transaction.belongsTo(models.Document);

                Transaction.belongsTo(models.Company, {
                    as: 'OwnerTransaction',
                    foreignKey: 'owner_transaction_id',
                    constraints: false
                });
                Transaction.belongsToMany(models.Condition, {
                    as: 'Conditions',
                    through: {model: models.ConditionTransaction, unique: false},
                    foreignKey: "transactionId",
                    unique: false,
                    constraints: false
                });
                Transaction.belongsToMany(models.Decision, {
                    as: 'Decisions',
                    through: {model: models.DecisionTransaction, unique: false},
                    foreignKey: "transactionId",
                    unique: false,
                    constraints: false
                });
                Transaction.belongsToMany(models.Company, {
                    as: 'Members',
                    through: {model: models.MemberTransaction, unique: false},
                    foreignKey: "transactionId",
                    unique: false,
                    constraints: false
                });
            }
        }
    });
    Transaction.hook('beforeUpdate', function (transaction) {
        if (transaction._previousDataValues.status != "pending") {
            throw new Error("You can't change transaction!")
        }
    });
    return Transaction;
};
