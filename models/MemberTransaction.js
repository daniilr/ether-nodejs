module.exports = function(sequelize, DataTypes) {
    var MemberTransaction = sequelize.define('MemberTransaction', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        companyId: {
            allowNull: false,
            type: DataTypes.INTEGER
        },
        transactionId: {
            allowNull: false,
            type: DataTypes.INTEGER
        }
    }, {
        timestamps: false,
        classMethods: {
            associate: function(models) {
                // associations can be defined here
            }
        }
    });
    return MemberTransaction;
};