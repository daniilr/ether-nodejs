/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
    var Condition = sequelize.define('Condition', {
        id : {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        condition: {
            type: DataTypes.ENUM('active', 'closed','broken'),
            allowNull: false,
            defaultValue: 'active'
        },
        comment: {
            type: DataTypes.STRING,
            allowNull: true
        }

    }, {
        paranoid: true,
        classMethods: {
            associate: function (models) {
                Condition.belongsToMany(models.Transaction, {
                    as: 'ConditionTransaction',
                    through: {model: models.ConditionTransaction, unique: false},
                    foreignKey: "conditionId",
                    unique: false,
                    constraints: false
                });
                Condition.belongsTo(models.Company, {
                    onDelete: "CASCADE",
                    foreignKey: {
                        allowNull: false
                    }
                });
            }
        }
    });

    Condition.hook('beforeUpdate', function (condition) {
        console.log(condition);
        if (condition._previousDataValues.condition != "active") {
            throw new Error("You can't change condition!")
        }
    });

    return Condition;
};
