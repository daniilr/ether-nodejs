module.exports = function (sequelize, DataTypes) {
    var ConditionTransaction = sequelize.define('ConditionTransaction', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        conditionId: {
            allowNull: false,
            type: DataTypes.INTEGER
        },
        transactionId: {
            allowNull: false,
            type: DataTypes.INTEGER
        }
    }, {
        timestamps: false,
        classMethods: {
            associate: function (models) {
                // associations can be defined here
            }
        }
    });
    return ConditionTransaction;
};