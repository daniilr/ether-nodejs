/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {

    var Wallet = sequelize.define('Wallet', {
        count_local: {
            type: DataTypes.INTEGER,
            allowNull: false,
            defaultValue: 0
        },
        count_blockchain: {
            type: DataTypes.INTEGER,
            allowNull: false,
            defaultValue: 0
        },
        balance: {
            type: DataTypes.DOUBLE,
            allowNull: false,
            defaultValue: 0
        },
        pass: {
            type: DataTypes.STRING,
            allowNull: false
        },
        addressBlock: {
            type: DataTypes.STRING,
            allowNull: true
        },
        address: {
            type: DataTypes.STRING,
            allowNull: true
        }
    });

    return Wallet;
};
