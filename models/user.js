var env = process.env.NODE_ENV || 'development';
var config = require('../config/config.json')[env];
var bcrypt = require('bcrypt');


module.exports = function (sequelize, DataTypes) {
    var User = sequelize.define('User', {
        email: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: {msg: "E-mail is already in use!"},
            validate: {
                isEmail: {
                    msg: "E-mail is incorrect!"
                }
            }
        },
        password: {
            type: DataTypes.STRING,
            allowNull: false,
            set: function (val) {
                var salt = bcrypt.genSaltSync(config.saltRounds);
                var hash = bcrypt.hashSync(val, salt);
                this.setDataValue('password', hash);
            }
        },
        confirm: {
            type: DataTypes.STRING,
            defaultValue: true
        },
        blocked: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false
        },
        deleted: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false
        },
        verified: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false
        }
    }, {
        paranoid: true,
        classMethods: {
            associate: function (models) {
                User.hasOne(models.Profile);
                User.hasOne(models.Company);
                User.hasMany(models.File)
            }
        },
        instanceMethods: {
            validPassword: function (password) {
                return bcrypt.compareSync(password, this.password);
            }
        }
    });
    return User;
};
