/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {

    var Company = sequelize.define('Company', {
        id : {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING,
            allowNull: true
        },
        country: {
            type: DataTypes.INTEGER,
            allowNull: true
        },
        data: {
            type: DataTypes.JSONB,
            allowNull: true
        },
        verified: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false
        }
    }, {
        paranoid: true,
        classMethods: {
            associate: function (models) {
                Company.belongsToMany(models.Transaction, {
                    as: 'MemberTransaction',
                    through: {model: models.MemberTransaction,unique: false},
                    foreignKey: "companyId",
                    unique: false,
                    constraints: false
                });
                Company.hasOne(models.Wallet);
                Company.hasOne(models.Logo);
            }
        }
    });
    return Company;
};