/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
    var Logo = sequelize.define('Logo', {
        hash: {
            type: DataTypes.STRING,
            allowNull: true,
            unique: {msg: "Photo is already in use!"}
        },
        small: {
            type: DataTypes.STRING,
            allowNull: true
        },
        normal: {
            type: DataTypes.STRING,
            allowNull: true
        },
        big: {
            type: DataTypes.STRING,
            allowNull: true
        },
        original: {
            type: DataTypes.STRING,
            allowNull: false
        }
    }, {
        paranoid: true
    });
    return Logo;
};
