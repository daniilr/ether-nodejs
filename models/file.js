/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
    var File = sequelize.define('File', {
        uuid: {
            type: DataTypes.STRING,
            allowNull: true
        },
        filename: {
            type: DataTypes.STRING,
            allowNull: true
        },
        mimetype: {
            type: DataTypes.STRING,
            allowNull: true
        },
        size: {
            type: DataTypes.INTEGER,
            allowNull: true,
        },
        hash: {
            type: DataTypes.STRING,
            allowNull: true
        },
        destination: {
            type: DataTypes.STRING,
            allowNull: true
        }
    }, {
        paranoid: true,
        classMethods: {
            associate: function (models) {
                File.belongsTo(models.User, {
                    onDelete: "CASCADE",
                    foreignKey: {
                        allowNull: true
                    }
                });
                File.belongsTo(models.Document, {
                    onDelete: "CASCADE",
                    foreignKey: {
                        allowNull: false
                    }
                });
            }
        }
    });
    return File;
};
