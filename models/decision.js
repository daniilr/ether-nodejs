/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
    var Decision = sequelize.define('Decision', {
        id : {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        action: {
            type: DataTypes.ENUM('approve', 'decline', 'pending'),
            allowNull: false,
            defaultValue: 'pending'
        },
        comment: {
            type: DataTypes.STRING,
            allowNull: true
        }

    }, {
        paranoid: true,
        classMethods: {
            associate: function (models) {
                Decision.belongsToMany(models.Transaction, {
                    as: 'DecisionTransaction',
                    through: {model: models.DecisionTransaction, unique: false},
                    unique: false,
                    constraints: false,
                    foreignKey: "decisionId"
                });
                Decision.belongsTo(models.Company, {
                    onDelete: "CASCADE",
                    foreignKey: {
                        allowNull: false
                    }
                });
            }
        }
    });

    Decision.hook('beforeUpdate', function (decision) {
        console.log(decision);
        if (decision._previousDataValues.action != "pending") {
            throw new Error("You can't change decision!")
        }
    });

    return Decision;
};
