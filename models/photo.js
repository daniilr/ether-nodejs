/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
    var Photo = sequelize.define('Photo', {
        hash: {
            type: DataTypes.STRING,
            allowNull: true,
            unique: {msg: "Photo is already in use!"}
        },

        small: {
            type: DataTypes.STRING,
            allowNull: true
        },
        normal: {
            type: DataTypes.STRING,
            allowNull: true
        },
        big: {
            type: DataTypes.STRING,
            allowNull: true
        },
        original: {
            type: DataTypes.STRING,
            allowNull: true
        }
    }, {
        paranoid: true,
        classMethods: {
            associate: function (models) {
                Photo.belongsTo(models.Profile, {
                    onDelete: "CASCADE",
                    foreignKey: {
                        allowNull: true
                    }
                });
            }
        }
    });
    return Photo;
};
