module.exports = function (sequelize, DataTypes) {
    var DecisionTransaction = sequelize.define('DecisionTransaction', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        decisionId: {
            allowNull: false,
            type: DataTypes.INTEGER
        },
        transactionId: {
            allowNull: false,
            type: DataTypes.INTEGER
        }
    }, {
        timestamps: false,
        classMethods: {
            associate: function (models) {
                // associations can be defined here
            }
        }
    });
    return DecisionTransaction;
};