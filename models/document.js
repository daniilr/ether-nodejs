/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
    var Document = sequelize.define('Document', {
        name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        sign: {
            type: DataTypes.DATE,
            allowNull: false
        }
    }, {
        paranoid: true,
        classMethods: {
            associate: function (models) {
                Document.hasMany(models.File)
            }
        }
    });
    return Document;
};
